# assign full health
bob_health = 10
bob_hits = []
jane_health = 10
jane_hits = []

rounds = 0
game_on = true
while game_on do

  rounds += 1
  
  # bob hits jane
  damage = rand(0..10)
  bob_hits << damage
  jane_health -= damage # subtract damage

  # jane hits bob
  damage = rand(0..10)
  jane_hits << damage
  bob_health -= damage # subtract damage
 
  # test health
  if jane_health < 0 
   puts "jane died"
   game_on = false
  end
  if bob_health < 0
    puts "bob died"
    game_on = false
  end

end

# print the final score
puts "Stats"
puts "They went #{rounds} rounds \n\n"
puts "Jane: #{jane_health}"
puts "Bob: #{bob_health}"
puts "Bob's hits #{bob_hits}"
puts "Jane's hits #{jane_hits}"
