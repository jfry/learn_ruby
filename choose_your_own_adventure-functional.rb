def chapter_header(chapter_number)
   puts "\n\n------------------------"
   puts "Chapter #{chapter_number}"
   puts "------------------------\n\n"
end

# welcome
puts "Welcome to choose your own demise"
puts "What is your name?"
name = gets.chomp

# say hello
puts "Hello #{name}"

# chapter 1
chapter_header(1)
puts "Do you want to a. hang yourself or b. take tons of pills?"
answer = gets.chomp

if answer == "a"
   puts "the rope broke, you didn't die"
else 
   puts "you died"
   exit
end


# chapter 2
chapter_header(2)
puts "do you want to a. fight a tiger or b. swallow a sword?"
answer = gets.chomp

puts "either way you died"