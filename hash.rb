my_hash = {
	"name" => "Josh",
	"age" => 29,
	"gay" => false
}

name = my_hash["name"]
age = my_hash["age"]

puts "#{name} is #{age} years old"

if my_hash["gay"] == true
	puts my_hash["name"] + " is gay"
else
	puts my_hash["name"] + " is not gay"
end