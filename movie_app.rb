movies = {
    "Psycho" => "5",
    "The Birds" => "5",
    "Lifeboat" => "3",
    "North by Northwest" => "4",
    "Dial M for Murder" => "2"
}

puts "What would you like to do?"
puts "-- [1] add a movie."
puts "-- [2] update a movie."
puts "-- [3] display all movies."
puts "-- [4] delete a movie."

choice = gets.chomp
case choice

when "1"
    puts "Movie title:"
    title = gets.chomp    
    if movies[title.to_sym].nil?
        puts "How would you rate #{title}?"
        rating = gets.chomp
        movies[title.to_sym] = rating.to_i

        puts "#{title} was added with a #{rating} star rating."
    else
        puts "This title is already here"
    end
when "2"
    puts "What movie do you want to update?"
    title = gets.chomp
    if movies[title.to_sym].nil?
        puts "Add a new rating for #{title}"
        rating = gets.chomp
        movies[title] = rating.to_i
    else
        puts "That movie does not exist in your collection."
    end
when "3"
    movies.each { |movie,rating| puts "#{movie}: #{rating}" }
when "4"
    puts "What movie do you want to delete?"
    title = gets.chomp
    if movies[title.to_sym].nil?
        movies.delete(title)
        puts "#{title} was removed"
    else
        puts "That movie does not exist in your collection."
    end
else
    puts "Error!"
end