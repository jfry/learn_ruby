# assign full health
bob_health = 10
jane_health = 10

# bob hits jane
damage = rand(0..10)
jane_health -= damage # subtract damage

# jane hits bob
damage = rand(0..10)
bob_health -= damage # subtract damage

puts "The score"
puts "Jane: #{jane_health}"
puts "Bob: #{bob_health}"
